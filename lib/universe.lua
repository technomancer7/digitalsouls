config_data = nil

function conf()
    if config_data == nil then
        print("Loading config...")
        c = love.filesystem.read( "universe.json" )
        config_data = json.decode(c)
    end

    return config_data
end

function setConfig(key, value)
    c = conf()
    config_data[key] = value
    love.filesystem.write("universe.json", json.encode(c))
end

function getConfig(key)
    c = conf()
    return c[key]
end

function loadJson(path) return json.decode(love.filesystem.read( path )) end
