--local sandbox = setmetatable({ }, { __index = _G })

function exec(s)
    local lines = {}

    local fn, syntaxError = load(s, "code", 't', _G)
    if not fn then return syntaxError end

    local success, runtimeError = pcall(fn)
    if not success then return runtimeError end
	
    lines = table.concat(lines, '\n')
	if lines ~= "" then	return lines end
end

return exec;