require "lib.klua"
inspect = require "lib.inspect"
--require "lib.class"

CodeSpeak = {}
function CodeSpeak.new()  -- initializer function
    self = deepcopy(CodeSpeak)
    self.variables = {}
    self.variables["_lineNumber"] = -1
    self.variables["_parserMode"] = ""
    self.variables["_namespace"] = "default"
    self.variables["_strict"] = true
    self.variables["_showLn"] = true
    --self:fileHandle("this", "lib.codespeak.lua")
    --self.variables["#this"]:read()
    --self.variables["#this"]:write()
    
    --self.labelRegister = {}
    self.signals = {}
    --print(self.variables[":ln"])
    self.lines = {}
    self.commands = {
        echo = { scope = {"runtime", "repl"}, execute = function(engine, s) engine:echo(s) end },
        comp = { scope = {"compile"}, execute = function(engine, s) engine:echo(s) end },
        addi = {
            scope = {"runtime", "repl"}, usage = "addi x y z",
            help = "Adds number x + y, and saves to variable z.\nx and y can be either variable names, file handle, or raw number, as long as the result is a number.",
            execute = function(engine, ln) 
                --print(inspect(self.variables))
                local x = tonumber(self:contentsOf(ln:SplitStr(" ")[1]))
                local y = tonumber(self:contentsOf(ln:SplitStr(" ")[2]))
                local z = ln:SplitStr(" ")[3]
                --print(x, y)
                if engine:get("_strict") and engine:get(z) == nil then
                    engine:broadcast("error", "variable_not_defined")
                    return
                end

                if tonumber(x) == nil then engine:broadcast("error", "value_not_number(x)"); return end;
                if tonumber(y) == nil then engine:broadcast("error", "value_not_number(y)"); return end;
                local sum = x+y

                engine:writeTo(z, sum)
            end
        },
        subi = {
            scope = {"runtime", "repl"}, usage = "subi x y z",
            help = "Subtracts number x - y, and saves to variable z.\nx and y can be either variable names, file handle, or raw number, as long as the result is a number.",
            execute = function(engine, ln) 
                local x = tonumber(self:contentsOf(ln:SplitStr(" ")[1]))
                local y = tonumber(self:contentsOf(ln:SplitStr(" ")[2]))
                local z = ln:SplitStr(" ")[3]

                if engine:get("_strict") and engine:get(z) == nil then
                    engine:broadcast("error", "variable_not_defined")
                    return
                end

                if tonumber(x) == nil then engine:broadcast("error", "value_not_number(x)"); return end;
                if tonumber(y) == nil then engine:broadcast("error", "value_not_number(y)"); return end;
                local sum = x-y

                engine:writeTo(z, sum)
            end
        },
        divi = {
            scope = {"runtime", "repl"}, usage = "divi x y z",
            help = "Divides number x / y, and saves to variable z.\nx and y can be either variable names, file handle, or raw number, as long as the result is a number.",
            execute = function(engine, ln) 
                local x = tonumber(self:contentsOf(ln:SplitStr(" ")[1]))
                local y = tonumber(self:contentsOf(ln:SplitStr(" ")[2]))
                local z = ln:SplitStr(" ")[3]

                if engine:get("_strict") and engine:get(z) == nil then
                    engine:broadcast("error", "variable_not_defined")
                    return
                end

                if tonumber(x) == nil then engine:broadcast("error", "value_not_number(x)"); return end;
                if tonumber(y) == nil then engine:broadcast("error", "value_not_number(y)"); return end;
                local sum = x/y

                engine:writeTo(z, sum)
            end
        },
        muli = {
            scope = {"runtime", "repl"}, usage = "muli x y z",
            help = "Multiply number x * y, and saves to variable z.\nx and y can be either variable names, file handle, or raw number, as long as the result is a number.",
            execute = function(engine, ln) 
                local x = tonumber(self:contentsOf(ln:SplitStr(" ")[1]))
                local y = tonumber(self:contentsOf(ln:SplitStr(" ")[2]))
                local z = ln:SplitStr(" ")[3]

                if engine:get("_strict") and engine:get(z) == nil then
                    engine:broadcast("error", "variable_not_defined")
                    return
                end

                if tonumber(x) == nil then engine:broadcast("error", "value_not_number(x)"); return end;
                if tonumber(y) == nil then engine:broadcast("error", "value_not_number(y)"); return end;
                local sum = x*y

                engine:writeTo(z, sum)
            end
        },
        mark = {
            scope = {"compile"}, help = "Marks current line as a label called n.", usage = "mark n",
            execute = function(engine, ln) engine:set("@"..ln, engine:lineNumber(), 1) end
        },
        jump = {
            scope = {"runtime"}, help = "Moves parser to label called n.", usage = "jump n",
            execute = function(engine, ln)                
                local num = engine:get("@"..ln)
                if num ~= nil then
                    self:setLineNumber(num)
                else
                    engine:error("invalid_label_name("..ln..")")
                end
            end
        },
        test = {},
        doif = {
            scope = {"runtime"}, usage = "doif op1 type op2, command",
            help = [[Does command if check passes. 
            The op values are variable pointers, file pointers, strings, or numbers.

            Example: doif x == 1, echo "X is 1!"]],
            execute = function(engine, ln)
                f, _ = ln:find(",")
                local cmd = ln:sub(f+2, #ln)
                ln = ln:SplitStr(",")[1]
                local op1 = engine:contentsOf(ln:SplitStr(" ")[1])
                local op2 = engine:contentsOf(ln:SplitStr(" ")[3])
                local mtype = ln:SplitStr(" ")[2]
                if mtype == "=" or mtype == "==" or mtype == "eq" then
                    if op1 == op2 then engine:parse(cmd) end
                end
                if mtype == "<" or mtype == "lt" then
                    if op1 < op2 then engine:parse(cmd) end
                end
                if mtype == ">" or mtype == "gt" then
                    if op1 > op2 then engine:parse(cmd) end
                end
                if mtype == ">=" or mtype == "gte" then
                    if op1 >= op2 then engine:parse(cmd) end
                end
                if mtype == "<=" or mtype == "lte" then
                    if op1 <= op2 then engine:parse(cmd) end
                end
            end
        },
        grab = {},
        drop = {},
        setv = {
            scope = {"runtime", "repl"},
            help = "Defines variable k to have value v.",
            usage = "setv k v",
            execute = function(engine, ln) 
                local vn = ln:SplitStr(" ")[1]
                local a = ln:sub(#vn+2)
                if engine:get("_strict") and engine:get(vn) == nil then
                    engine:broadcast("error", "variable_not_defined")
                    return
                end
                
                engine:set(vn, a)
            end
        },
        defn = {
            scope = {"compile"},
            help = "Defines a variable as an empty string with name v.",
            usage = "defn v",
            execute = function(engine, ln) 
                local vn = ln:SplitStr(" ")[1]
                local a = ln:sub(#vn+2)
                engine:set(vn, a)
            end
        },
        rem = { scope = {"*"}, execute = function(engine, ln) end;},
        halt = { scope = {"*"}, execute = function(engine, ln) os.exit() end; },
        sleep = { scope = {"*"}, execute = function(engine, ln) sleep(tonumber(ln)) end; },
        help = {
            scope = {"repl"},
            execute = function(engine, ln) 
                if engine.commands[ln] ~= nil then
                    local c = engine.commands[ln]
                    engine:setShowLn(false)
                    engine:setNamespace("help")
                    if c.usage then engine:writeln(c.usage) else engine:writeln(ln) end
                    if c.help then engine:writeln(c.help) end
                    if c.scope then engine:writeln(table.concat(c.scope, "|")) end
                    
                    engine:setShowLn(true)
                    engine:setNamespace("default")
                else

                end
            end;
        }
    }
    self.commands["-"] = self.commands["rem"]
    self.commands["//"] = self.commands["rem"]
    self.commands["?"] = self.commands["if"]
    self.commands[":"] = self.commands["mark"]
    self.sym = {"-", "//", "?", ":"}

    self:addSignal("error", "def", function(ln)
        self:writeln("!!!!!!!")
        self:writeln("ERR: "..ln)
        self:writeln("!!!!!!!")
    end)
    self:addSignal("stdout", "def", function(ln) self:writeln(ln) end)
    return self
end

function CodeSpeak.error(self, name) self:broadcast("error", name) end

function CodeSpeak.isSystemVar(self, name)
    -- @ is label regstry, _ is internal variables
    if name:hasPrefix("@") or name:hasPrefix("_") then return true end
    return false
end

function CodeSpeak.fileHandle(self, name, path)
    if not name:hasPrefix("#") then name = "#"..name end
    local file = io.open( path, "r+" )
    local content = ""
    if file ~= nil then 
        content = file:read( "*all" )
        file:close()
    end
    self.variables[name] = {
        path = path,
        body = content
    }
end

function CodeSpeak.dropFileHandle(self, name)
    if not name:hasPrefix("#") then name = "#"..name end
    self.variables[name] = nil
end
function CodeSpeak.writeTo(self, name, text)
    --handle IF FILE then write to file, else write to variable name
    if name:hasPrefix("#") then
        local file = io.open(self.variables[name].path, "w+")
        file:write(text)
        file:close()
    else
        if self:isSystemVar(name) then self:error("access_prohibited_name"); return end
        self.variables[name] = text
    end
end

function CodeSpeak.appendTo(self, name, text)
    --handle IF FILE then write to file, else write to variable name
    if name:hasPrefix("#") then
        local file = io.open(self.variables[name].path, "w+")
        local contents = file:read( "*all" )
        file:write(contents.."\n"..text)
        file:close()
    else
        if self:isSystemVar(name) then self:error("access_prohibited_name"); return end
        self.variables[name] = self.variables[name].."\n"..text
    end
end

function CodeSpeak.prependTo(self, name, text)
    --handle IF FILE then write to file, else write to variable name
    if name:hasPrefix("#") then
        local file = io.open(self.variables[name].path, "w+")
        local contents = file:read( "*all" )
        file:write(text.."\n"..contents)
        file:close()
    else
        if self:isSystemVar(name) then self:error("access_prohibited_name"); return end
        self.variables[name] = text.."\n"..self.variables[name]
    end
end

function CodeSpeak.replaceIn(self, name, oldt, newt)
    --handle IF FILE then write to file, else write to variable name
    if name:hasPrefix("#") then
        local file = io.open(self.variables[name].path)
        local contents = file:read( "*all" )
        file:write(contents:gsub(newt, oldt))
        file:close()
    else
        if self:isSystemVar(name) then self:error("access_prohibited_name"); return end
        self.variables[name] = self.variables[name]:gsub(newt, oldt)
    end
end

function CodeSpeak.addSignal(self, stype, id, fn)
    if self.signals[stype] == nil then self.signals[stype] = {} end
    --if self.signals[stype][id] ~= nil then
    self.signals[stype][id] = fn
    --end
end

function CodeSpeak.signal(self, stype, id, ln)
    if self.signals[stype] == nil then return end
    if self.signals[stype][id] ~= nil then
        self.signals[stype][id](ln)
    end
end

function CodeSpeak.broadcast(self, stype, ln)
    if self.signals[stype] == nil then return end
    for _, value in pairs(self.signals[stype]) do
        value(ln)
    end
end

function CodeSpeak.setMode(self, newm) return self:set("_parserMode", newm, 1) end;
function CodeSpeak.setLineNumber(self, ln) return self:set("_lineNumber", ln, 1) end;
function CodeSpeak.setStrict(self, s) return self:set("_strict", s, 1) end;
function CodeSpeak.setDebug(self, dbg) return self:set("_debug", dbg, 1) end;
function CodeSpeak.setNamespace(self, ns) return self:set("_namespace", ns, 1) end;
function CodeSpeak.setShowLn(self, ns) return self:set("_showLn", ns, 1) end;

function CodeSpeak.showLn(self) return self:get("_showLn") end;
function CodeSpeak.namespace(self) return self:get("_namespace") end;
function CodeSpeak.mode(self) return self:get("_parserMode") end;
function CodeSpeak.debug(self) return self:get("_debug") end;
function CodeSpeak.lineNumber(self) return tonumber(self:get("_lineNumber")) end;
function CodeSpeak.strict(self) return self:get("_strict") end;


function CodeSpeak.parse(self, line) -- main entrypoint function to process text line
    for c = 1, #self.sym do
        local sym = self.sym[c]
        if line:hasPrefix(sym) then
            line = line:gsub(sym, sym.." ", 1)
        end
    end
    local cmd = line:SplitStr(" ")[1]:lower()
    local arg = line:sub(#cmd+2, #line)

    -- HERE handle sub-codes in the arg line
    -- find ${code} and handle

    if self.commands[cmd] ~= nil then
        if self.commands[cmd].scope == nil or has_value(self.commands[cmd].scope, self:mode()) or has_value(self.commands[cmd].scope, "*") then
            self.commands[cmd].execute(self, arg)
        end
    end
end;

function CodeSpeak.set(self, key, var, override)
    if self:isSystemVar(key) and override == nil then self:error("access_prohibited_name("..key..")"); return end
    self.variables[key] = tostring(var)
end;
function CodeSpeak.get(self, key) return self.variables[key] end;

function CodeSpeak.getType(self, ln)
    ln = tostring(ln)
    if ln:hasPrefix('"') and ln:hasSuffix('"') then return "string" end
    if ln:hasPrefix("'") and ln:hasSuffix("'") then return "string" end
    if ln:hasPrefix('`') and ln:hasSuffix('`') then return "string" end
    if ln == "true" or ln == "false" then return "bool" end
    if tonumber(ln) ~= nil then return "number" end
    if ln:hasPrefix("#") then return "file" end
    return "name"
end

function CodeSpeak.boolify(self, ln)
    if type(ln) == "boolean" then return ln end
    if self:getType(ln) == "bool" then
        if table.contains({"1", "yes", "true", "t", "y"}, ln) then
            return true
        else
            return false
        end
    end

    return false
end

function CodeSpeak.contentsOf(self, val)
    if self:getType(val) == "string" then return val:sub(2, #val-1) end
    if self:getType(val) == "number" then return tonumber(val) end
    if self:getType(val) == "bool" then return self:boolify(val) end
    if self:getType(val) == "file" then return self.variables[val].body end
    if self:getType(val) == "name" then return self.variables[val] end
end

function CodeSpeak.echo(self, ln)
    self:writeln(self:contentsOf(ln))
end;
function CodeSpeak.writeln(self, ln)
    -- todo colours
    local ns = self:namespace()
    --print(self:showLn(), type(self:showLn()))
    if self:boolify(self:showLn()) == true then ns = ns.." ("..self:lineNumber()..")" end
    print(ns, "|   "..tostring(ln))
end;

function CodeSpeak.loadFile(self, path)
    local file = io.open( path, "r" )
    local contents = file:read( "*all" )
    self.lines = contents:SplitStr("\n")
    file:close()
end
    
function CodeSpeak.compile(self)
    self:setMode("compile")
    self:doLines()
end
function CodeSpeak.run(self)
    self:setMode("runtime")
    self:doLines()
end

function CodeSpeak.doLines(self)
    self:setLineNumber(0)

    while self:lineNumber() < #self.lines do
        self:setLineNumber(self:lineNumber()+1)
        self:parse(self.lines[self:lineNumber()])
    end
end
    --[[addCommand = function(self, name, fn) end;
    loadText = function(self, txt) end;
    loadFile = function(self, path) end;
    compile = function(self) end;

    -- if file register (#name), get contents of file
    -- if variable (plain name), get variable
    -- if string (ends and starts with quote marks), just get plain string
    contentOf = function(self, id) end;

    -- new idea for file register, make them assign to a variable
    -- distinction of file handle with normal variables will be file handle starts with #
    createFileRegister = function(self, name, path) end;
    dropFileRegister = function(self, name) end;
}]]--

--return CodeSpeak