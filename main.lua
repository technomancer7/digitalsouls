json = require "lib.json"
inspect = require "lib.inspect"
klua = require 'lib.klua'
class = require "lib.clasp"
lib_path = love.filesystem.getSource( ).."/lib"

require "flow-control"
require "audio-control"
require "mouse-control"
require "lib.timing"
require "lib.universe"
require "cascade"

require "campaign.overworld"
ticks = 0
raw_ticks = 0
game_seconds = 0
raw_game_seconds = 0
game_dt = 0
paused = false

function pause() paused = true end
function unpause() paused = false end

function execMenuList()
    if main_menu_select == 0 then
        goToOverworld()
    elseif main_menu_select == 1 then
        print("Playlist not implemented")
    else
        print("Undefined menu option")
    end
end

function activateMainMenu()
    print("Opening main menu")
    main_menu_select = 0
    registerClickable("main_menu_opt1", 0, 120, 100, 120)
    addClickableEvent("main_menu_opt1", function() execMenuList() end)
    addClickableHover("main_menu_opt1", function() main_menu_select = 0 end)
    registerClickable("main_menu_opt2", 0, 120, 120, 140)
    addClickableEvent("main_menu_opt2", function() execMenuList() end)
    addClickableHover("main_menu_opt2", function() main_menu_select = 1 end)

    setHook("tick", "main_menu", function()
        love.graphics.print("Welcome to Digital Souls!", 0, 0, 0, 1)
    end)

    setHook("keypressed", "main_menu", function(key)
        --print(key)
        if key == "up" then
            if main_menu_select <= 0 then return end
            main_menu_select = main_menu_select - 1
        elseif key == "down" then
            if main_menu_select >= 1 then return end
            main_menu_select = main_menu_select + 1
        elseif key == "return" then
            execMenuList()
        end
    end)

    setHook("unhook", "main_menu", function()
        unHook("tick", "main_menu")
        unHook("keypressed", "main_menu")
        removeClickable("main_menu_opt1")
        removeClickable("main_menu_opt2")
    end)

    setScene("main_menu", main_menu_scene)
end

function main_menu_scene()
    love.graphics.print("Welcome to Digital Souls!", 100, 20, 0, 1)
    love.graphics.print(">1.STORY", 0, 100, 0, 1)
    love.graphics.print(">2.PLAYLIST", 0, 120, 0, 1)

    if main_menu_select == 0 then
        love.graphics.print("<", 120, 100, 0, 1)
    elseif main_menu_select == 1 then
        love.graphics.print("<", 120, 120, 0, 1)
    end
end

function love.load()
    delays = getConfig("delays")
    activateMainMenu()
    local bgms = love.filesystem.getDirectoryItems( "audio/bgm/" )
    for c = 1, #bgms do
        local fn = bgms[c] -- audio.mp3
        local id = fn:SplitStr("%.")[1] --audio
        audio["bgm"][id] = love.audio.newSource("audio/bgm/"..fn, "stream")
    end
    local sfxs = love.filesystem.getDirectoryItems( "audio/sfx/" )
    for c = 1, #sfxs do
        local fn = sfxs[c] -- audio.mp3
        local id = fn:SplitStr("%.")[1] --audio
        audio["sfx"][id] = love.audio.newSource("audio/sfx/"..fn, "static")
    end

    -- GRAPHICS --
    font = love.graphics.newFont( "graphics/FiraCode-Bold.ttf" )
    love.graphics.setFont( font )

    playBgm("zero")
end

function love.update(dt)
    --if paused then return end
    game_dt = game_dt + dt
    if game_dt >= 1 then
        raw_game_seconds = raw_game_seconds + 1
        game_dt = 0
        if paused ~= false then 
            game_seconds = game_seconds + 1 
            execHooks("seconds", game_seconds)
        end
    end

    raw_ticks = raw_ticks + 1

    if paused ~= false then return end
    ticks = ticks + 1

    if #delays > 0 then
        local dl = shallowcopy(delays)
        for c = 1, #dl do
            if dl[c]["time"] == ticks then
                print("Execute "..dl[c]["name"])
                executeGlobal("test")
                table.remove(delays, c)
                setConfig("delays", delays)
                return
            end
        end
    end
    handleClickableHover()
    execHooks("tick", ticks)
end

function getMouseBounds(xs, xe, ys, ye)
    if love.mouse.getX() > xs and love.mouse.getX() < xe and love.mouse.getY() > ys and love.mouse.getY() < ye then
        return true
    else
        return false
    end
end

function love.mousepressed( x, y, button, istouch, presses )
    handleClickableEvent()
    execHooks("mousepressed", {x = x, y = y, button = button, istouch = istouch, presses = presses})

end

function love.draw()
    if paused and raw_game_seconds % 2 == 0 then
        love.graphics.print("PAUSED", 0, 0)
    elseif paused and raw_game_seconds % 2 ~= 0 then
        love.graphics.print({{155, 155, 0}, "PAUSED"}, 0, 0)
    end
    if scene ~= nil then
        scene["fn"]()
    end
    execHooks("draw")
end

function love.keypressed(key)
    if key == "p" and paused == true then unpause(); return end
    if key == "p" and paused == false then pause(); return end

    if paused == true then return end
    execHooks("keypressed", key)
end
