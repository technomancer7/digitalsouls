clickables = {}

function registerClickable(id, xs, xe, ys, ye)
    clickables[id] = {id = id, xs = xs, xe = xe, ys = ys, ye = ye, hover = nil, click = nil, enabled = true}
    return clickables[id]
end

function addClickableEvent(id, fn)
    clickables[id].click = fn
end

function addClickableHover(id, fn)
    clickables[id].hover = fn
end

function editClickable(id, key, val)
    clickables[id][key] = val
end

function removeClickable(id)
    clickables[id] = nil
end

function handleClickableEvent()
    for k, v in pairs(clickables) do
        if getMouseBounds(v.xs, v.xe, v.ys, v.ye) then
            if love.mouse.isDown( 1 ) and v.click ~= nil then
                v.click()
            end
        end
    end
end

function handleClickableHover()
    for k, v in pairs(clickables) do
        if getMouseBounds(v.xs, v.xe, v.ys, v.ye) then
            if v.hover ~= nil then v.hover() end
        end
    end
end