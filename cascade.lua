
-- Current runtime data and configuration, json serialisable state that can be exported
cascade = {
    base_speed = 1,
    sprint_mod = 3,
    spawn_point = {0, 0},
    max_x = 23,
    max_y = 17,
    panel_size = 32,
    grid = {},
    player = {x = 0, y = 0, health = 3},
    sprites = {
        player = love.graphics.newImage("graphics/char.jpg"),
        harm = love.graphics.newImage("graphics/harm.jpg"),
        warn = love.graphics.newImage("graphics/warning.jpg")
    },
    events = {
        hurt_player = function()
            print("Player was hit. (TODO)")
        end
    },
    entities = {}
}

function cascade.spawn(x, y)
    local en = {
        -- location
        x = x,
        y = y,

        -- data for sprite
        sprite = cascade.sprites.harm,

        -- function to run when player collides
        on_collide = cascade.events.hurt_player,

        -- the AI script, what the entity will do
        script = nil,

        -- generic health 
        health = 1,

        -- if not nil, reduces by 1 each game_second proc. destroys when 0
        lifespan = nil,

        -- time the entity was spawned
        spawned_at = game_seconds,

        -- text to display over the sprite, and offsets to control position
        label = "",
        label_offset_x = -10,
        label_offset_y = 30,

        -- entities unique ID, can be changed arbitrarily. <cascade>,get_entity uses this to find it
        id = #cascade.entities+1
    }

    table.insert(cascade.entities, en)
    return en
end

function cascade.entity(id)
    for k, v in pairs(cascade.entities) do
        print("CHECK", v.id, v.label)
        if v.id == id then return v end
    end
end

function cascade.setSpawnPoint(x, y)
    cascade.spawn_point = {x, y}
end

function cascade.get_move_speed()
    local speed = cascade.base_speed
    if love.keyboard.isDown("lshift") or love.keyboard.isDown("rshift") then
        speed = speed + cascade.sprint_mod
    end
    return speed
end

-- game x,y to screen x,y
function cascade.coord(x, y)
    return {cascade.grid[x.."."..y].screen_x, cascade.grid[x.."."..y].screen_y}
end

function cascade.init(x_size, y_size)
    if x_size == nil then x_size = 23 end
    if y_size == nil then y_size = 17 end
    -- Define size of the grid
    cascade.max_x = x_size
    cascade.max_y = y_size

    -- Player spawn point, dead centre
    cascade.player.x = cascade.spawn_point[1]
    cascade.player.y = cascade.spawn_point[2]


    -- Creating pointers to the centre of each panel on the grid
    -- First one starts at the half of one panel, then we increment it
    local scrx = cascade.panel_size / 2
    local scry = cascade.panel_size / 2

    for x = 0,cascade.max_x,1 do
        scry = cascade.panel_size / 2
        for y = 0,cascade.max_y,1 do
            cascade.grid[x.."."..y] = {screen_x = scrx, screen_y = scry}
            scry = scry + cascade.panel_size
        end
        scrx = scrx + cascade.panel_size
    end

    -- setHook("tick", "cascade", function() end)
    setHook("keypressed", "cascade", function(key)
        if key == "left" then
            cascade.player.x = cascade.player.x - cascade.get_move_speed()
            if cascade.player.x < 0 then cascade.player.x = 0 end
        elseif key == "right" then
            cascade.player.x = cascade.player.x + cascade.get_move_speed()
            if cascade.player.x > cascade.max_x then cascade.player.x = cascade.max_x end
        elseif key == "up" then
            cascade.player.y = cascade.player.y - cascade.get_move_speed()
            if cascade.player.y <= 0 then cascade.player.y = 0 end
        elseif key == "down" then
            cascade.player.y = cascade.player.y + cascade.get_move_speed()
            if cascade.player.y > cascade.max_y then cascade.player.y = cascade.max_y end
        end
    end)

    setHook("draw", "cascade", function()
        local cd = cascade.coord(cascade.player.x, cascade.player.y)
        love.graphics.draw(cascade.sprites.player, cd[1], cd[2])

        for k, v in pairs(cascade.entities) do 
            local ec = cascade.coord(v.x, v.y)
            love.graphics.draw(v.sprite, ec[1], ec[2])
            --if v.label ~= "" then love.graphics.print(v.label, ec[1]+v.label_offset_x, ec[2]+v.label_offset_y, 0, 1) end
            if v.label ~= "" then 
                love.graphics.printf(v.label, ec[1]+v.label_offset_x, ec[2]+v.label_offset_y, 50, "center") 
            end
        end
    end)

    setHook("tick", "cascade", function()
        for k, v in pairs(cascade.entities) do 
            if v.x == cascade.player.x and v.y == cascade.player.y then
                v.on_collide()
            end
        end
    end)
    setHook("unhook", "cascade", function()
        unHook("tick", "cascade")
        unHook("draw", "cascade")
        unHook("keypressed", "cascade")
        cascade.entities = {}
        cascade.grid = {}
    end)
end

