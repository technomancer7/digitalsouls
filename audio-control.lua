audio = {bgm = {}, sfx = {}, now_playing = nil}
function playBgm(id)
    if audio["now_playing"] ~= nil then love.audio.stop(audio["bgm"][audio["now_playing"]]) end
    love.audio.play(audio["bgm"][id])
    audio["now_playing"] = id
end

function stopBgm()
    if audio["now_playing"] ~= nil then love.audio.stop(audio["bgm"][audio["now_playing"]]) end
    audio["now_playing"] = nil
end

function playSfx(id)
    love.audio.stop(audio["sfx"][id])
    love.audio.play(audio["sfx"][id])
end

function stackSfx(id)
    love.audio.play(audio["sfx"][id])
end